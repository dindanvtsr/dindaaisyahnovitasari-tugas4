import React from 'react';
import {View} from 'react-native';

import Routing from './src/Routing';

export default function App() {
  return (
    <View style={{flex: 1}}>
      <Routing />
    </View>
  );
}
