import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Profile from './screen/Profile';
import EditProfile from './screen/EditProfile';
import FAQ from './screen/FAQ';

const Stack = createNativeStackNavigator();

export default function ProfileNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="ProfilePage" component={Profile} />
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{headerShown: true, headerTitle: 'Edit Profile'}}
      />
      <Stack.Screen
        name="FAQ"
        component={FAQ}
        options={{headerShown: true, headerTitle: 'FAQ'}}
      />
    </Stack.Navigator>
  );
}
