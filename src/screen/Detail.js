import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground,
} from 'react-native';

const Detail = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <ImageBackground
            source={require('../assets/image/headerDetail.png')}
            style={{
              width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
              height: 317,
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableOpacity
                onPress={() => navigation.navigate('BottomNav')}>
                <Image
                  source={require('../assets/icon/back.png')}
                  style={styles.iconAtas}></Image>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('HomeNavigation', {
                    screen: 'Keranjang',
                  })
                }>
                <Image
                  source={require('../assets/icon/WhiteBag.png')}
                  style={styles.iconAtas}></Image>
              </TouchableOpacity>
            </View>
          </ImageBackground>
          <View style={styles.isiKontenPutih}>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 19}}>
              Jack Repair Seturan
            </Text>
            <Image
              source={require('../assets/icon/ratings.png')}
              style={{width: 63, height: 11, marginTop: 5}}
            />
            <View style={styles.lokasi}>
              <Image
                source={require('../assets/icon/location.png')}
                style={{width: 24, height: 24}}
              />
              <View style={{width: '75%'}}>
                <Text style={styles.teksLokasi}>
                  Jalan Affandi (Gejayan), No.15, Sleman Yogyakarta, 55384
                </Text>
              </View>
              <View>
                <Text style={styles.teksMaps}>Lihat Maps</Text>
              </View>
            </View>
            <View style={styles.keteranganToko}>
              <Text style={styles.keteranganTokoBuka}>BUKA</Text>
              <Text style={styles.keteranganJam}>09:00 - 21:00</Text>
            </View>
          </View>
          <View style={styles.border}></View>
          <View style={styles.isiKontenPutih2}>
            <View>
              <Text style={styles.judulDesc}>Deskripsi</Text>
              <Text style={styles.dalamDesc}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
                gravida mattis arcu interdum lectus egestas scelerisque. Blandit
                porttitor diam viverra amet nulla sodales aliquet est. Donec
                enim turpis rhoncus quis integer. Ullamcorper morbi donec
                tristique condimentum ornare imperdiet facilisi pretium
                molestie.
              </Text>
            </View>
            <View>
              <Text style={styles.judulDesc}>Range Biaya</Text>
              <Text style={styles.dalamDesc}>Rp 20.000 - 80.000</Text>
            </View>
            <TouchableOpacity
              style={styles.repairButton}
              onPress={() =>
                navigation.navigate('HomeNavigation', {screen: 'FormPemesanan'})
              }>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16.3,
                  fontWeight: 'bold',
                }}>
                Repair Disini
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  isiKontenPutih: {
    width: '100%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 19,
    borderTopRightRadius: 19,
    paddingHorizontal: 33,
    paddingTop: 25,
    marginTop: -20,
  },
  isiKontenPutih2: {
    width: '100%',
    paddingHorizontal: 33,
  },
  lokasi: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: -4,
    marginTop: 13,
  },
  teksLokasi: {
    fontSize: 12,
    color: '#979797',
    marginLeft: 7,
  },
  teksMaps: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#3471CD',
  },
  keteranganToko: {
    flexDirection: 'row',
    marginTop: 13,
  },
  keteranganTokoBuka: {
    backgroundColor: '#11a84e1f',
    color: '#11A84E',
    fontWeight: 'bold',
    borderRadius: 10.5,
    width: 65,
    padding: 3,
    textAlign: 'center',
    fontSize: 12,
  },
  keteranganJam: {
    fontWeight: 'bold',
    color: '#343434',
    fontSize: 12,
    marginTop: 3,
    marginLeft: 15,
  },
  judulDesc: {
    color: '#201F26',
    fontSize: 16,
    marginTop: 23,
  },
  dalamDesc: {
    color: '#595959',
    fontSize: 14,
    marginTop: 6,
  },
  border: {
    borderColor: '#EEE',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    marginTop: 13,
  },
  repairButton: {
    width: '100%',
    marginTop: 35,
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 35,
  },
  iconAtas: {
    width: 24,
    height: 24,
    marginHorizontal: 26,
    marginTop: 28,
  },
});

export default Detail;
