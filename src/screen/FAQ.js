import React, {useState} from 'react';
import {
  View,
  Text,
  LayoutAnimation,
  StyleSheet,
  UIManager,
  Platform,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

const Accordion = ({title, children}) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleOpen = () => {
    setIsOpen(value => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  return (
    <>
      <TouchableOpacity
        onPress={toggleOpen}
        style={styles.heading}
        activeOpacity={0.6}>
        {title}
        <Icon
          name={isOpen ? 'chevron-up-outline' : 'chevron-down-outline'}
          size={18}
          color="black"
        />
      </TouchableOpacity>
      <View style={[styles.list, !isOpen ? styles.hidden : undefined]}>
        {children}
      </View>
    </>
  );
};

const FAQ = () => {
  const title = (
    <View>
      <Text style={styles.sectionTitle}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit?
      </Text>
    </View>
  );
  const body = (
    <View>
      <Text style={styles.sectionDescription}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit?
      </Text>
    </View>
  );
  return (
    <View style={styles.all}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 50}}>
        <View>
          <View style={styles.kotakSummary}>
            <Accordion title={title}>{body}</Accordion>
          </View>
          <View style={styles.kotakSummary}>
            <Accordion title={title}>{body}</Accordion>
          </View>
          <View style={styles.kotakSummary}>
            <Accordion title={title}>{body}</Accordion>
          </View>
          <View style={styles.kotakSummary}>
            <Accordion title={title}>{body}</Accordion>
          </View>
          <View style={styles.kotakSummary}>
            <Accordion title={title}>{body}</Accordion>
          </View>
          <View style={styles.kotakSummary}>
            <Accordion title={title}>{body}</Accordion>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default FAQ;

const styles = StyleSheet.create({
  heading: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  hidden: {
    height: 0,
  },
  list: {
    overflow: 'hidden',
  },
  sectionTitle: {
    fontSize: 16,
    color: '#000',
    fontWeight: '500',
  },
  sectionDescription: {
    fontSize: 16,
    color: '#595959',
    fontWeight: '500',
  },
  all: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  kotakSummary: {
    backgroundColor: '#fff',
    marginTop: 33,
    paddingVertical: 16,
    paddingHorizontal: 26,
    marginBottom: -17,
  },
});
