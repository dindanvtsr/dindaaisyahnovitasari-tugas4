import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

const FormPemesanan = ({navigation}) => {
  const [merk, onChangeMerk] = React.useState('');
  const [warna, onChangeWarna] = React.useState('');
  const [ukuran, onChangeUkuran] = React.useState('');
  const [catatan, onChangeCatatan] = React.useState('');
  const [sol, setSol] = useState(false);
  const [jahit, setJahit] = useState(false);
  const [repaint, setRepaint] = useState(false);
  const [cuci, setCuci] = useState(false);

  return (
    <View style={styles.all}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 50}}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <View style={styles.isiForm}>
            <Text style={styles.label}>Merek</Text>
            <TextInput
              style={styles.input}
              onChangeText={onChangeMerk}
              value={merk}
              placeholder="Masukkan Merk Barang"
            />
            <Text style={styles.label}>Warna</Text>
            <TextInput
              style={styles.input}
              onChangeText={onChangeWarna}
              value={warna}
              placeholder="Warna Barang, cth : Merah - Putih "
            />
            <Text style={styles.label}>Ukuran</Text>
            <TextInput
              style={styles.input}
              onChangeText={onChangeUkuran}
              value={ukuran}
              placeholder="Cth : S, M, L / 39,40"
            />
            <Text style={styles.label}>Photo</Text>
            <View style={styles.cameraContainer}>
              <Image
                source={require('../assets/icon/camera.png')}
                style={styles.camera}></Image>
              <Text style={styles.addPhoto}>Add Photo</Text>
            </View>
            <View style={styles.checkboxContainer}>
              <View style={styles.checkboxLabel}>
                <CheckBox
                  disabled={false}
                  value={sol}
                  onValueChange={newValue => setSol(newValue)}
                />
                <Text style={styles.checkboxLabelText}>Ganti Sol Sepatu</Text>
              </View>
              <View style={styles.checkboxLabel}>
                <CheckBox
                  disabled={false}
                  value={jahit}
                  onValueChange={newValue => setJahit(newValue)}
                />
                <Text style={styles.checkboxLabelText}>Jahit Sepatu</Text>
              </View>
              <View style={styles.checkboxLabel}>
                <CheckBox
                  disabled={false}
                  value={repaint}
                  onValueChange={newValue => setRepaint(newValue)}
                />
                <Text style={styles.checkboxLabelText}>Repaint Sepatu</Text>
              </View>
              <View style={styles.checkboxLabel}>
                <CheckBox
                  disabled={false}
                  value={cuci}
                  onValueChange={newValue => setCuci(newValue)}
                />
                <Text style={styles.checkboxLabelText}>Cuci Sepatu</Text>
              </View>
            </View>
            <Text style={styles.label}>Catatan</Text>
            <TextInput
              style={styles.inputCatatan}
              onChangeText={onChangeCatatan}
              value={catatan}
              placeholder="Cth : ingin ganti sol baru"
            />
            <TouchableOpacity
              style={styles.masukkanButton}
              onPress={() =>
                navigation.navigate('HomeNavigation', {screen: 'Keranjang'})
              }>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16.3,
                  fontWeight: 'bold',
                }}>
                Masukkan Keranjang
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    backgroundColor: '#fff',
    flex: 1,
  },
  back: {
    width: 24,
    height: 24,
    marginLeft: 21,
    marginTop: 19,
  },
  headerTitle: {
    color: '#201F26',
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 10,
    marginTop: 17,
    marginBottom: 22,
  },
  isiForm: {
    marginHorizontal: 20,
  },
  label: {
    color: '#BB2427',
    fontWeight: '600',
    fontSize: 12,
    marginTop: 25,
  },
  input: {
    backgroundColor: '#F6F8FF',
    borderRadius: 7.24,
    marginTop: 11,
    paddingLeft: 9,
  },
  camera: {
    width: 20,
    height: 18,
  },
  cameraContainer: {
    width: '25%',
    height: '10%',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#BB2427',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 11,
  },
  masukkanButton: {
    width: '100%',
    marginTop: 35,
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 35,
  },
  addPhoto: {
    color: '#BB2427',
    fontSize: 12,
    marginTop: 10,
  },
  inputCatatan: {
    backgroundColor: '#F6F8FF',
    borderRadius: 7.24,
    marginTop: 11,
    paddingLeft: 9,
    paddingTop: 14,
    paddingBottom: 63,
  },
  checkboxLabel: {
    flexDirection: 'row',
  },
  checkboxLabelText: {
    marginTop: 7,
    color: '#201F26',
    fontSize: 14,
    marginLeft: 10,
  },
  checkboxContainer: {
    marginTop: 33,
    marginBottom: 20,
  },
});

export default FormPemesanan;
